m3 = [3*n for n in range(1,334) if 3*n < 1000]
m5 = [5*n for n in range(1,(1000/5 + 1)) if 5*n < 1000]

def prods(mult,lim):
	return [mult*n for n in range(1,int(lim/mult)+1) if mult*n < lim]

d = OrderedDict()
for i in sorted(m3+m5):
    d[i] = 1


def sieveOfSundaram(n):
    k = int(( n - 2 ) / 2 )
    a = [0] * ( k + 1 )
    for i in range( 1, k + 1):
        j = i
        while(( i + j + 2 * i * j ) <= k):
            a[ i + j + 2 * i * j ] = 1
            j+=1
    if n < 2:
    	return []
    primes = [2]
    for i in range(1, k + 1):
        if a[i] == 0:
            primes += [2 * i +1]
    return primes

def nFibs(n):
	fibs = [1,2]
	for i in range(n):
        fibs += [fibs[-1]+fibs[-2]]
	return fibs

def limFibs(lim):
	fibs = [1,2]
	while True:
        n = fibs[-1]+fibs[-2]
        if n > lim:
        	break
        fibs += [n]
	return fibs
